/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32l0xx_hal.h"

/* USER CODE BEGIN Includes */
#include "string.h"
#include "pt_cornell_1_2_2.h"

#define PT_DELAY(var, time) do {var = HAL_GetTick() + time; PT_WAIT_WHILE(pt, HAL_GetTick() < var);} while(0)


/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
TIM_HandleTypeDef htim22;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_TIM22_Init(void);                                    
void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);
                                

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
uint8_t flag;
uint8_t cnt = 0;
uint8_t reset_timeout_clap_flag = 0;
uint8_t exti_clap_flag = 0;

#define WAIT_ISR_FLAG      1
#define CLAP_ISR_RXED_FLAG 2
#define START_TIMEOUT_FLAG 4


static enum {
    WAIT_LOGIN = 0,
    WAIT_PASS,
} state = WAIT_LOGIN;

void login_pass_fsm()
{

    switch(state) 
    {
      case WAIT_PASS:
        {
          if (3 == cnt) {
            state = WAIT_LOGIN;
            HAL_UART_Transmit(&huart2, (uint8_t*)"\r\npass ok", strlen("\r\npass ok"), 100);
            
            relay_out_GPIO_Port->ODR ^= relay_out_Pin;
            
          } else if (2 == cnt) {
            state = WAIT_PASS;
            HAL_UART_Transmit(&huart2, (uint8_t*)"\r\nlogin2 ok", strlen("\r\nlogin2 ok"), 100);
          }
          
          else {
            state = WAIT_LOGIN;
            HAL_UART_Transmit(&huart2, (uint8_t*)"\r\npass bad", strlen("\r\npass bad"), 100);
          }
        }
      break;    
      default:
        {
          if (2 == cnt) {
            state = WAIT_PASS;
            HAL_UART_Transmit(&huart2, (uint8_t*)"\r\nlogin ok", strlen("\r\nlogin ok"), 100);
          }
        }
      break;
    }
}


PT_THREAD(get_data_thread(struct pt* pt))
{
    static uint32_t t32 = 0;  
    static uint8_t t8 = 0;
    
    PT_BEGIN(pt);
    
    if (flag) {        
        flag = 0;
        reset_timeout_clap_flag = 1;
        
        t32 = HAL_GetTick() + 1000;
        while(t32 > HAL_GetTick()) {
          
            if (flag) {
                flag = 0;
                reset_timeout_clap_flag = 1;
                
                
                t32 = HAL_GetTick() + 1000;
            }
            PT_YIELD(pt);
        } 
        
        for (t8 = cnt; t8; t8--) {
            
            led_out_GPIO_Port->ODR |= led_out_Pin;
            if ((2 == cnt) || (3 == cnt)) {HAL_TIM_PWM_Start(&htim22, TIM_CHANNEL_2);}
            PT_DELAY(t32, 100);        
            
            led_out_GPIO_Port->ODR &= ~led_out_Pin;
            if ((2 == cnt) || (3 == cnt)) {HAL_TIM_PWM_Stop(&htim22, TIM_CHANNEL_2);}            
            PT_DELAY(t32, 100);                    
        }        
        HAL_UART_Transmit(&huart2, (uint8_t*)&cnt, 1, 100);

        login_pass_fsm();   
        cnt = 0;
        reset_timeout_clap_flag = 0;
        flag = 0;
        exti_clap_flag = 0;
    }
    PT_END(pt);
}


PT_THREAD(pass_timeout_thread(struct pt* pt))
{
    static uint32_t t32 = 0;  
    static uint8_t t8 = 0;  
    
    PT_BEGIN(pt);
    
    while(1) {
        
        PT_YIELD(pt);
        
        if (reset_timeout_clap_flag) {
            reset_timeout_clap_flag = 0;
            for(t8 = 10; t8; t8--) {
                PT_DELAY(t32, 1000);
                
                if (reset_timeout_clap_flag) {
                    reset_timeout_clap_flag = 0;
                    t8 = 10;
                }
            }
            state = WAIT_LOGIN;
            HAL_UART_Transmit(&huart2, (uint8_t*)"\r\nstate is wait login", strlen("\r\nstate is wait login"), 100);
        }
        
    }

    PT_END(pt);
}

PT_THREAD(exti_driver_thread(struct pt* pt))
{
    static uint32_t timestamp = 0;

    PT_BEGIN(pt);

    while (1) {

        PT_YIELD(pt);

        if (exti_clap_flag) {

            PT_DELAY(timestamp, 200);

            HAL_UART_Transmit(&huart2, (uint8_t*)"\r\nclap", strlen("\r\nclap"), 100);
            cnt++;
            flag = 1;
            exti_clap_flag = 0;
        }
    }

    PT_END(pt);
}

/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */
  struct pt get_data_pt;
  struct pt pass_timeout_pt;
  struct pt exti_driver_pt;
  
  PT_INIT(&get_data_pt);
  PT_INIT(&pass_timeout_pt);
  PT_INIT(&exti_driver_pt);
  
  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  MX_TIM22_Init();

  /* USER CODE BEGIN 2 */


  
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  
  HAL_TIM_PWM_Start(&htim22, TIM_CHANNEL_2);
  HAL_Delay(200);
  HAL_TIM_PWM_Stop(&htim22, TIM_CHANNEL_2);
  
  cnt = 0;
  reset_timeout_clap_flag = 0;
  while (1)
  {
      get_data_thread(&get_data_pt);
      pass_timeout_thread(&pass_timeout_pt);
      exti_driver_thread(&exti_driver_pt);
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLLMUL_4;
  RCC_OscInitStruct.PLL.PLLDIV = RCC_PLLDIV_2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* TIM22 init function */
static void MX_TIM22_Init(void)
{

  TIM_MasterConfigTypeDef sMasterConfig;
  TIM_OC_InitTypeDef sConfigOC;

  htim22.Instance = TIM22;
  htim22.Init.Prescaler = 319;
  htim22.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim22.Init.Period = 100;
  htim22.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  if (HAL_TIM_PWM_Init(&htim22) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim22, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 50;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim22, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  HAL_TIM_MspPostInit(&htim22);

}

/* USART2 init function */
static void MX_USART2_UART_Init(void)
{

  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(led_out_GPIO_Port, led_out_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(relay_out_GPIO_Port, relay_out_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : led_out_Pin */
  GPIO_InitStruct.Pin = led_out_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(led_out_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : mic_in_Pin */
  GPIO_InitStruct.Pin = mic_in_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(mic_in_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : relay_out_Pin */
  GPIO_InitStruct.Pin = relay_out_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(relay_out_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI4_15_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI4_15_IRQn);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void _Error_Handler(char * file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler_Debug */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
